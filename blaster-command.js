/*
Blaster Command
Command-line parser for node.
(c) 2014 Alex McClung
*/

var mArgs = [];
var mDisplayLength = 0;

exports.add = function(shortForm, longForm, defaultValue, helpText)
{
    if( "" == shortForm )
        shortForm = null;

    if( "" == longForm )
        longForm = null;
    
    var display = "";

    if(null != shortForm && null != longForm)
    {
        display = "-" + shortForm + ", --" + longForm;
    }
    else if(null != shortForm)
    {
        display = "-" + shortForm;
    }
    else if(null != longForm)
    {
        display = "--" + longForm;
    }
    else
    {
        console.log("Attempted to add a command-line argument with no short or long form");
        process.exit(0);
    }

    if(null == helpText)
        helpText = "";

    if(display.length > mDisplayLength ) 
        mDisplayLength = display.length;

    var sf = null != shortForm ? shortForm.replace("=ARG", "") : null;
    var lf = null != longForm  ? longForm.replace("=ARG", "") : null;

    mArgs.push( {display:display, shortForm:sf, longForm:lf, helpText:helpText, defaultValue:defaultValue}  );
}

exports.help = function()
{
    console.log("Help:");
    var leadTab = "  ";
    var space = "    ";

    for(var i=0, l=mArgs.length; i<l; i++)
    {
        if( "" != mArgs[i].helpText )
        {
            var disp = mArgs[i].display;
            while(disp.length < mDisplayLength )
                disp = " " + disp;

            console.log( leadTab + disp + space + mArgs[i].helpText )

            if(null != mArgs[i].defaultValue)
            {
                var disp = " ";
                while(disp.length < mDisplayLength)
                    disp = " " + disp;
                console.log(leadTab + disp + space + "Default: " + mArgs[i].defaultValue);
            }
        }
    }
}

exports.parse = function()
{
	var err = false;
	var args = {};

    var arg,   // Argument's name
        value, // Argument's value
        beg,   // Where the argument's string actually begins (after taking out all '-' characters)
        split; // Where to split the argument from the value
    var isShort = false;
	for(var i=2,l=process.argv.length; i<l && false == err; i++)
	{
        arg = process.argv[i];
        if('-' == arg[0])
        {
            // In other words: if there's another dash, start looking at index 2 instead of 1
            beg   = (arg[1] == '-') ? 2 : 1;
            split = arg.indexOf("=");

            // Here, the user specified a valid command-line argument but no value. 
            // That's probably ok, so we just say that it's value is true
            if( split < 0 )
            {
                arg   = arg.substr(beg);
                value = true;
            }
            else
            {
                value = (split+1 >= arg.length) ? null : arg.substr(split+1);
                arg   = arg.substr(beg, split-beg);
            }

            isShort = arg.length == 1;
            if( null == value )
            {
                value = isShort ? _GetDefaultValueForShortArg(arg) : _GetDefaultValueForLongArg(arg);
            }

            if( null == value )
            {
                console.log("Error procesing argument " + arg + " - no value was given and could not find a default argument.");
                err = true;
            }
            else
            {
                // We have a value hooray. Insert it into the dictionary.
                args[arg]=value;

                // Also look for a matching argument in the dicationary and assign the alternate form to this value as well
                var altForm = _GetAlternateForm(arg);
                if(null != altForm)
                    args[altForm] = value;
            }
        }
        else
        {
            console.log("Error: unprocessable command-line argument at index " + i + ": " + arg);
            err = true;
        }
	}

    // Finished regular command processing. Now we need to go through and inject things with default values
    for(var i=0, l=mArgs.length; i<l; i++)
    {
        var argShort = mArgs[i].shortForm;
        var argLong  = mArgs[i].longForm;
        if( null != argShort && null == args[argShort] )
        {
            args[argShort] = _GetDefaultValueForShortArg(argShort);
            if( null != argLong )
                args[argLong] = args[argShort]
        }
        else if( null != argLong && null == args[argLong] )
        {
            args[argLong] = _GetDefaultValueForLongArg(argLong);
        }
    }

    if(err)
    {
        process.exit(1);
    }

    return args;
}

function _GetDefaultValueForShortArg(arg)
{
    for(var i=0; i < mArgs.length; i++)
        if(mArgs[i].shortForm == arg)
            return mArgs[i].defaultValue;
    return null;
}

function _GetDefaultValueForLongArg(arg)
{
    for(var i=0, l=mArgs.length; i < l; i++)
        if(mArgs[i].longForm == arg)
            return mArgs[i].defaultValue;
    return null;
}

function _GetAlternateForm(arg)
{
    // If this is a short arg, return the long form
    if(arg.length == 1)
    {
        for(var i=0, l = mArgs.length; i < l; i++)
            if(mArgs[i].shortForm == arg)
                return mArgs[i].longForm;
    }
    // This is a long arg, return the short form
    else
    {
        for(var i=0, l = mArgs.length; i < l; i++)
            if(mArgs[i].longForm == arg)
                return mArgs[i].shortForm;
    }
    return null;
}