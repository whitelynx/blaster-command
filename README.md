Blaster-Command
=============

Command-line arguments parser for node.

Installation
===

    npm install blaster-command

Usage
===

Load the blaster-command module with the standard require statement.

    var parser = require('blaster-command');
    
You add (or "register") command-line arguments with the add function.

    parser.add( "h",     "help",                false,       "Display Help" );

The first parameter identifies the short-form of the argument. In this case, "-h" will be searched for in the command-line.

The second is the long-form of the argument. In this case, "--help" will be searched for in the command-line.

NOTE: The parser will throw an error and terminate the process if you do not specify at least one of these parameters.

The third parameter is the argument's default value if it isn't found in the command line. In this example, the help value will default to "false."

The fourth parameter defines the help text to display when parser.showHelp is invoked. This will be explained later.

To parse the command lines, invoke the .parse method. It will return a dictionary containing the values from the command-line, as well as default values for any arguments that were absent.

    var commands = parser.parse();
    console.log( commands );

If you were to run this:

    node mysite.js -h

You would see this in the output:

    { h: true, help: true }

If you were to run this:

    node mysite.js

You would see this in the output:

    { h: false, help: false }

    
As you can see, both the long and short forms of the argument are present in the returned dictionary. When the values are absent, blaster-command will insert any default values that you specified in your calls to add.

You will also note that these values are either true (if present in the command line) or false (if absent and given a default value).

If you want to capture argument values from the comand-line, use "=ARG" in your command expressions like this:

    parser.add( "p=ARG", "port=ARG",            8080,        "Sets the HTTP port that the server listens on." );

If you were to run this:

    node mysite.js -p=10001

The output would be:

    { h: false, help: false, p:10001, port:10001 }

Again, note how both forms of the arguments are present.

Here is a sample script that demonstrates the built-in help:

    var parser = require('blaster-command');
    parser.add( "h",     "help",                false,       "Display Help" );
    parser.add( "p=ARG", "port=ARG",            8080,        "Sets the HTTP port that the server listens on." );
    parser.add( "d",     "debug",               false,       "Enable debug code." )

    var commands = parser.parse();
    
    if( commands.help )
    {
        parser.showHelp();
    }

Output:

    Help:
              -h, --help    Display Help
                            Default: false
      -p=ARG, --port=ARG    Sets the HTTP port that the server listens on.
                            Default: 8080
             -d, --debug    Enable debug code.
                            Default: false

License 
===

(The MIT License)

Copyright (c) 2014 Alex McClung

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
'Software'), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
